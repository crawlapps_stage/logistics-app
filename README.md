
# Changes for env file

APP_NAME
APP_URL

DB_HOST
DB_DATABASE
DB_USERNAME
DB_PASSWORD

QUEUE_CONNECTION=database

SHOPIFY_API_KEY
SHOPIFY_API_SECRET

TOOKAN_API_KEY
TOOKAN_API_URL

# Fire this commands to setup the project

composer install

npm install

php artisan migrate

npm run prod

