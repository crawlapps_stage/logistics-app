import {ContextualSaveBar, Modal, Button, Toast } from '@shopify/app-bridge/actions';

export default {
    showToast(message) {
        const toastOptions = {
            message: message,
            duration: 2000,
        };
        const toastNotice = Toast.create(window.shopify_app_bridge, toastOptions);
        toastNotice.dispatch(Toast.Action.SHOW);
    },
    confirmationModal(i, base) {
        const okButton = Button.create(window.shopify_app_bridge, { label: 'Delete', style: Button.Style.Danger });
        okButton.subscribe(Button.Action.CLICK, () => {
            base.deleteForm(i), base.submit(), myModal.dispatch(Modal.Action.CLOSE)
        });

        const cancelButton = Button.create(window.shopify_app_bridge, { label: 'Cancel' });
        cancelButton.subscribe(Button.Action.CLICK, () => {
            myModal.dispatch(Modal.Action.CLOSE)
        });

        const modalOptions = {
            title: 'warning !',
            message: 'Are you sure you ant to delete this from ?',
            footer: {
                buttons: {
                    primary: okButton,
                    secondary: [cancelButton],
                },
            },
        };
        const myModal = Modal.create(window.shopify_app_bridge, modalOptions);
        myModal.dispatch(Modal.Action.OPEN)
    },
    showSavebar(base) {
        if (JSON.stringify(base.spareData) != JSON.stringify(base.data)) {
            base.save.dispatch(ContextualSaveBar.Action.SHOW);
        }
        if (JSON.stringify(base.spareData) == JSON.stringify(base.data)) {
            base.save.dispatch(ContextualSaveBar.Action.HIDE);
        }
    },
    hideSavebar(base) {
        base.save.dispatch(ContextualSaveBar.Action.HIDE);
    },
    saveBar(base) {
        const options = {
            saveAction: {
                disabled: false,
                loading: false,
            },
            discardAction: {
                disabled: false,
                loading: false,
                discardConfirmationModal: true,
            },
        };

        const contextualSaveBar = ContextualSaveBar.create(window.shopify_app_bridge, options);
        base.save = contextualSaveBar;

        contextualSaveBar.set({ discardAction: { discardConfirmation: false } });

        contextualSaveBar.subscribe(ContextualSaveBar.Action.SAVE, function () {
            base.submit();
            contextualSaveBar.dispatch(ContextualSaveBar.Action.HIDE);
        });

        contextualSaveBar.subscribe(ContextualSaveBar.Action.DISCARD, function () {
            if (base.spareData != null) {
                let spare = base.spareData;
                base.data = base.spareData;
                base.spareData = JSON.parse(JSON.stringify(spare));
                contextualSaveBar.dispatch(ContextualSaveBar.Action.HIDE);
            }
        });
        contextualSaveBar.dispatch(ContextualSaveBar.Action.HIDE);
    },
    clearConsole(){
        if(process.env.MIX_APP_ENV != 'local'){
            // console.log(process.env.MIX_APP_ENV)
            console.clear();
        }
    }
}