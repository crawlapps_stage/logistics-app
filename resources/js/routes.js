
import Vue from 'vue';
import VueRouter from 'vue-router';

import Pickups from '../js/components/pages/pickup/index.vue';
import faq from './components/pages/faq/Index.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'pickups',
            title: 'Pickups',
            component: Pickups,
            meta : {
                ignoreInMenu : 0
            }
        },
        {
            path: '/faq',
            name: 'faq',
            title: 'Faq',
            component: faq,
            meta : {
                ignoreInMenu : 0
            }
        },
    ]
});

export default router;