<?php

namespace App\Listeners;

use App\Events\OrderFullfilled;
use App\Models\Pickup;
use App\Models\Responce;
use App\Models\Webhook;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;


class SendApiRequest implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderFullfilled  $event
     * @return void
     */
    public function handle(OrderFullfilled $event)
    {
        $webhookId = $event->webhookId;

        logger($webhookId);
        $webhook = Webhook::where('id', $webhookId)->first();

        $body = json_decode($webhook->body);
        $customer = $body->customer;

        $order_number = $body->order_number;
        $orderid = $body->id;
        $customer_email = $customer->email;
        $tags = $customer->tags;
        $customer_username = $customer->first_name . ' ' . $body->customer->last_name;
        $customer_phone = $customer->default_address->phone;
        $customer_address = $customer->default_address->address1 . ' ' . $body->customer->default_address->city  . ' ' . $body->customer->default_address->zip . ' ' . $body->customer->default_address->province . ' ' . $body->customer->default_address->country_name;
        $job_datetime = $body->fulfillments[0]->created_at;
        logger($job_datetime);

        $pickupDatas = Pickup::where('user_id', $webhook->user_id)->first();
        logger($pickupDatas);

        $pickupDatas = $pickupDatas->pickup_info;
        $apiResponce = [];
        $job_ids = [];

        foreach ($pickupDatas as $i => $pickupData) {

            $job_pickup_datetime = $job_datetime;
            $job_pickup_name = $pickupData['pickupName'];
            $job_pickup_email = $pickupData['pickupEmail'];
            $job_pickup_phone = $pickupData['pickupPhone'];
            $job_pickup_address = $pickupData['pickupAddress'];
            $job_description = $pickupData['jobDescription'];
            // $job_pickup_latitude = $pickupData['pickupLatitude'];
            // $job_pickup_longitude = $pickupData['pickupLongtitude'];
            $job_delivery_datetime = $job_datetime;
            $pickup_custom_field_template = $pickupData['pickupCustomeFieldTemplate'];

            $data = [
                "api_key" => env('TOOKAN_API_KEY'),
                "order_id" => $order_number,
                "job_description" => $job_description,
                "job_pickup_phone" => $job_pickup_phone,
                "job_pickup_name" => $job_pickup_name,
                "job_pickup_email" => $job_pickup_email,
                "job_pickup_address" => str_replace(".", "", $job_pickup_address),
                // "job_pickup_latitude" => $job_pickup_latitude,
                // "job_pickup_longitude" => $job_pickup_longitude,
                "job_pickup_datetime" => $job_pickup_datetime,
                "job_delivery_datetime" => $job_delivery_datetime,
                "pickup_custom_field_template" => $pickup_custom_field_template,
                "customer_email" => $customer_email,
                "customer_username" => $customer_username,
                "customer_phone" => $customer_phone,
                "customer_address" => str_replace(".", "", $customer_address),
                "tags" => $tags,
                "has_pickup" => "1",
                "has_delivery" => "1",
                "layout_type" => "0",
                "timezone" => "-330",
            ];

            logger($data);
            $responceData = Http::post(env('TOOKAN_API_URL'), $data);
            $apiResponce[] = json_decode($responceData);
        }

        foreach($apiResponce as $res){
            $job_ids[] = $res->data->job_id;
        }

        logger(json_encode($apiResponce));
        $responce = new Responce();
        $responce->user_id = $webhook->user_id;
        $responce->order_number = $order_number;
        $responce->shopify_order_id = $orderid;
        $responce->db_webhook_id = $webhookId;
        $responce->topic = $webhook->topic;
        $responce->body = json_encode($apiResponce);
        $responce->job_id = json_encode($job_ids);
        $confirm = $responce->save();

        if ($confirm) {
            return response()->json([
                "success" => true,
                "message" => "Api responce is saved in database."
            ], 200);
        }
    }
}
