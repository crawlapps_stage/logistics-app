<?php

namespace App\Listeners;

use App\Events\OrderUpdated;
use App\Models\Responce;
use App\Models\Webhook;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class UpdateTask
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\OrderUpdated  $event
     * @return void
     */
    public function handle(OrderUpdated $event)
    {
        logger($event->webhookId);

        $webhookId = $event->webhookId;

        $webhook = Webhook::find($webhookId);

        if ($webhook) {

            $webhookData = json_decode($webhook->body);

            if ($webhookData->fulfillment_status == 'fulfilled') {
                $order_id = $webhookData->id;
                $order_number = $webhookData->order_number;

                $responce = Responce::where('shopify_order_id', $order_id)->first();
                $bodies = json_decode($responce->body);

                $res = [];
                $job_ids = [];
                foreach ($bodies as $body) {
                    $job_id = $body->data->job_id;
                    $job_ids[] = $job_id;

                    $pickupData = [
                        "api_key" => env('TOOKAN_API_KEY'),
                        "customer_email" => $webhookData->customer->email,
                        "customer_username" => $webhookData->customer->default_address->name,
                        "customer_phone" => $webhookData->customer->default_address->phone,
                        "customer_address" => str_replace(".", "", $webhookData->customer->default_address->address1),
                        "tags" => $webhookData->tags,
                        "job_id" => $job_id,
                        "notify" => 1
                    ];

                    $responceData = Http::post(env('TOOKAN_UPDATE_URL'), $pickupData);
                    $res[] = json_decode($responceData);
                }
                logger($res);

                $responce = new Responce();
                $responce->user_id = $webhook->user_id;
                $responce->order_number = $order_number;
                $responce->shopify_order_id = $order_id;
                $responce->db_webhook_id = $webhookId;
                $responce->topic = $webhook->topic;
                $responce->body = json_encode($res);
                $responce->job_id = json_encode($job_ids);
                $confirm = $responce->save();

                if ($confirm) {
                    return response()->json([
                        "success" => true,
                        "message" => "Api responce is saved in database."
                    ], 200);
                }
            } else {
                return response()->json([
                    "success" => true,
                ], 200);
            }
        }
    }
}
