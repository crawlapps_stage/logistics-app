<?php

namespace App\Http\Controllers;

use App\Http\Requests\PickupRequest;
use App\Models\Pickup;
use App\Models\Responce;
use App\Models\Webhook;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class PickupController extends Controller
{
    public function addPickupData(PickupRequest $request)
    {
        try {

            $pickup = Pickup::where('user_id', Auth::user()->id)->first();

            if ($pickup) {
                $pickup->user_id = Auth::user()->id;
                $pickup->pickup_info = $request->data;
                $confirm = $pickup->save();

                if ($confirm) {
                    return response()->json([
                        "success" => true,
                        "message" => "Saved.",
                    ], 201);
                }
            }

            $pickup = new Pickup();
            $pickup->user_id = Auth::user()->id;
            $pickup->pickup_info = $request->data;
            $confirm = $pickup->save();

            if ($confirm) {
                return response()->json([
                    "success" => true,
                    "message" => "Saved.",
                ], 201);
            }
        } catch (Exception $err) {
            return response()->json([
                "success" => false,
                "message" => $err->getMessage(),
            ], 422);
        }
    }

    public function getPickupData()
    {
        try {
            $pickup = Pickup::where('user_id', Auth::user()->id)->first();

            if ($pickup) {
                return response()->json([
                    "success" => true,
                    "data" => $pickup,
                ], 200);
            } else {
                return response()->json([
                    "success" => false,
                    "data" => null,
                ], 200);
            }
        } catch (Exception $err) {
            return response()->json([
                "success" => false,
                "message" => $err->getMessage(),
            ], 422);
        }
    }

    public function getWebhooks()
    {
        try {
            $shop = Auth::user();

            $webhooks = $shop->api()->rest('GET', '/admin/api/2021-10/webhooks.json');
            dd($webhooks);
        } catch (Exception $err) {
            return response()->json([
                "success" => false,
                "message" => $err->getMessage(),
            ], 422);
        }
    }

    public function testApi()
    {

        try {

            $webhookId = 2;
            $webhook = Webhook::where('id', $webhookId)->first();
            $body = json_decode($webhook->body);
            // return($body);
            $customer = $body->customer;

            $order_number = $body->order_number;
            $orderid = $body->id;
            $customer_email = $customer->email;
            $tags = $customer->tags;
            $customer_username = $customer->first_name . ' ' . $body->customer->last_name;
            $customer_phone = $customer->default_address->phone;
            $customer_address = $customer->default_address->address1 . ' ' . $body->customer->default_address->city  . ' ' . $body->customer->default_address->zip . ' ' . $body->customer->default_address->province . ' ' . $body->customer->default_address->country_name;
            $job_datetime = $body->fulfillments[0]->created_at;

            $pickupDatas = Pickup::where('user_id', 1)->first();

            $pickupDatas = $pickupDatas->pickup_info;
            $apiResponce = [];
            $job_ids = [];
            
            foreach ($pickupDatas as $i => $pickupData) {

                $job_pickup_datetime = $job_datetime;
                $job_pickup_name = $pickupData['pickupName'];
                $job_pickup_email = $pickupData['pickupEmail'];
                $job_pickup_phone = $pickupData['pickupPhone'];
                $job_pickup_address = $pickupData['pickupAddress'];
                $job_description = $pickupData['jobDescription'];
                // $job_pickup_latitude = $pickupData['pickupLatitude'];
                // $job_pickup_longitude = $pickupData['pickupLongtitude'];
                $job_delivery_datetime = $job_datetime;
                $pickup_custom_field_template = $pickupData['pickupCustomeFieldTemplate'];

                $data = [
                    "api_key" => env('TOOKAN_API_KEY'),
                    "order_id" => $order_number,
                    "job_description" => $job_description,
                    "job_pickup_phone" => $job_pickup_phone,
                    "job_pickup_name" => $job_pickup_name,
                    "job_pickup_email" => $job_pickup_email,
                    "job_pickup_address" => str_replace(".", "", $job_pickup_address),
                    // "job_pickup_latitude" => $job_pickup_latitude,
                    // "job_pickup_longitude" => $job_pickup_longitude,
                    "job_pickup_datetime" => $job_pickup_datetime,
                    "job_delivery_datetime" => $job_delivery_datetime,
                    "pickup_custom_field_template" => $pickup_custom_field_template,
                    "customer_email" => $customer_email,
                    "customer_username" => $customer_username,
                    "customer_phone" => $customer_phone,
                    "customer_address" => str_replace(".", "", $customer_address),
                    "tags" => $tags,
                    "has_pickup" => "1",
                    "has_delivery" => "1",
                    "layout_type" => "0",
                    "timezone" => "-330",
                ];

                // return $data;
                $responceData = Http::post(env('TOOKAN_API_URL'), $data);
                $apiResponce[] = json_decode($responceData);
            }

            foreach($apiResponce as $res){
                $job_ids[] = $res->data->job_id;
            }

            // return (json_decode(json_encode($apiResponce)));
            $responce = new Responce();
            $responce->user_id = $webhook->user_id;
            $responce->order_number = $order_number;
            $responce->shopify_order_id = $orderid;
            $responce->db_webhook_id = $webhookId;
            $responce->topic = $webhook->topic;
            $responce->body = json_encode($apiResponce);
            $responce->job_id = json_encode($job_ids);
            $confirm = $responce->save();

            if ($confirm) {
                return response()->json([
                    "success" => true,
                    "message" => "Api responce is saved in database."
                ], 200);
            }
        } catch (Exception $th) {
            dd($th);
        }
    }
    
}
