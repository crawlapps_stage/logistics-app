<?php

namespace App\Http\Controllers\Api;

use App\Models\Responce;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class FullfillmentController extends Controller
{
    public function complateOrder(Request $request)
    {

        try {

            $job_id = $request->order_id;
            $responce = Responce::whereJsonContains('job_id', [intval($job_id)])->first();
            // return $responce;
            if ($responce) {
                $order_id = $responce->shopify_order_id;

                $shop = User::find($responce->user_id);
                if ($shop) {
                    $endPoint = '/admin/api/' . env('SHOPIFY_API_VERSION') . '/orders/' . $order_id . '.json';
                    $data = [
                        "order" => [
                            "id" => $order_id,
                            "tags" => "Completed"
                        ]
                    ];
                    $order = $shop->api()->rest('PUT', $endPoint, $data);
                    // dump($order);
                    if ($order) {
                        return response()->json([
                            "success" => true,
                            "message" => "Order Complated successfully."
                        ], 200);
                    }
                } else {
                    return response()->json([
                        "success" => false,
                        "message" => "User does not exist."
                    ], 422);
                }
            } else {
                return response()->json([
                    "success" => false,
                    "message" => "Order does not exist."
                ], 422);
            }
        } catch (Exception $err) {
            dd($err);
        }
    }
}
