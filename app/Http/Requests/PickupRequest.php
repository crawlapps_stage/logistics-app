<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PickupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            '*.*.pickupName' => 'required',
            '*.*.pickupEmail' => 'required|email',
            '*.*.pickupPhone' => 'required|numeric',
            // '*.*.pickupDate' => 'required',
            // '*.*.jobDeliveryDatetime' => 'required',
            '*.*.pickupAddress' => 'required',
            '*.*.jobDescription' => 'required',
            // '*.*.pickupLatitude' => 'required|numeric',
            // '*.*.pickupLongtitude' => 'required|numeric',
            '*.*.pickupCustomeFieldTemplate' => 'required',
        ];

        return($rules);

    }

    public function messages()
    {
        return [
            '*.*.pickupName.required' => 'Pickup Name is Required.',
            '*.*.pickupEmail.required' => 'Pickup Email is Required.',
            '*.*.pickupPhone.required' => 'Pickup Phone is Required.',
            // '*.*.pickupDate.required' => 'Pickup Date is Required.',
            // '*.*.jobDeliveryDatetime.required' => 'JobDelivery Datetime is Required.',
            '*.*.pickupAddress.required' => 'Pickup Address is Required.',
            '*.*.jobDescription.required' => 'Job Description is Required.',
            // '*.*.pickupLatitude.required|integer' => 'Pickup Latitude is Required.',
            // '*.*.pickupLongtitude.required|integer' => 'Pickup Longtitude is Required.',
            '*.*.pickupCustomeFieldTemplate.required' => 'Pickup CustomeField is Required.',

            '*.*.pickupEmail.email' => 'Please enter valid e-mail address.',

            '*.*.pickupPhone.numeric' => 'Please enter valid Phone number.',

            // '*.*.pickupDate.date' => 'Please enter valid Date.',
            // '*.*.jobDeliveryDatetime.date' => 'Please enter valid Date.',

            // '*.*.pickupLatitude.numeric' => 'Please enter valid Latitude.',
            // '*.*.pickupLongtitude.numeric' => 'Please enter valid Longtitude.',
        ];
    }
    
}
