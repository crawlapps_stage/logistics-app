<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PickupController;
use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::middleware('verify.shopify')->group(function () {
Route::get('/', function () {
    return view('app');
})->where('any', '.*')->middleware(['verify.shopify', 'secure.headers'])->name('home');
// });

Route::get('/login', [AuthController::class, 'index']);

Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
Route::get('testApi', [PickupController::class, 'testApi']);
Route::get('updateTest', [TestController::class, 'updateTest']);

Route::middleware(['verify.shopify', 'secure.headers'])->prefix('api')->group(function () {
    Route::post('/add-pickup-data', [PickupController::class, 'addPickupData'])->middleware(['verify.shopify'])->name('add-pickup-data');
    Route::get('/get-pickup-data', [PickupController::class, 'getPickupData'])->middleware(['verify.shopify'])->name('get-pickup-data');
    Route::get('/webhooks', [PickupController::class, 'getWebhooks'])->middleware(['verify.shopify'])->name('webhooks');
});

Route::get('flush', function () {
    request()->session()->flush();
});
