<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJobIdToResponcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('responces', function (Blueprint $table) {
            $table->json('job_id')->nullable();
            $table->unsignedBigInteger('order_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('responces', function (Blueprint $table) {
            $table->dropColumn('job_id');
            $table->dropColumn('order_number');
        });
    }
}
